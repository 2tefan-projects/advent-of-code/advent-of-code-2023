package main

import (
	"fmt"
	"os"
	"io"
	"math"
)

func main() {
	data, _ := io.ReadAll(os.Stdin)
	//fmt.Println(data)

	sum := 0
	tempLineNumber := 0
	pos := 0

	for i := len(data) - 1; i >= 0; i-- {
		char := data[i]

		if (char == 10) {
			fmt.Printf("New line detected -> number of previous line was: %d\n", tempLineNumber)
			sum += tempLineNumber
			tempLineNumber = 0
			pos = 0
		}

		if ! (48 <= char && char <= 57) {
			continue
		}

		number := char - 48
		tempLineNumber += int(number) * int(math.Pow10(pos))
		pos++
	}

	sum += tempLineNumber
	fmt.Println(sum)
}
