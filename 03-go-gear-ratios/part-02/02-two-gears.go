package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"regexp"
	//"runtime/pprof"
)

const LineLenght = 140
const InitalCapacitySlice = LineLenght + 10

func isNumber(value byte) bool {
	return '0' <= value && value <= '9'
}

func min(i1 int, i2 int) int {
	if i1 > i2 {
		return i2
	}
	return i1
}

func max(i1 int, i2 int) int {
	if i1 < i2 {
		return i2
	}
	return i1
}


func main() {
	//file, _ := os.Create("./cpu.pprof")
	//pprof.StartCPUProfile(file)
	//defer pprof.StopCPUProfile()

	reader := bufio.NewReader(os.Stdin)

	data := make([][]byte, 0, InitalCapacitySlice)
	gears := make([][][]int, 0, InitalCapacitySlice)
	numberRows := 0
	//numberColumns := -1

	for {
		row, err := reader.ReadBytes('\n')
		if err != nil {
			break
		}

		gearRow := make([][]int, 0, InitalCapacitySlice)
		for columnIndex := 0; columnIndex < len(row); columnIndex++ {
			char := row[columnIndex]

			if char == '*' {
				gearRow = append(gearRow, []int{1})
			} else { 
				gearRow = append(gearRow, []int{0})
			}
		}
		data = append(data, row)
		gears = append(gears, gearRow)

		numberRows++
	}

	//numberColumns = len(data[0])
	//fmt.Println(data)
	//fmt.Println(gears)
	r := regexp.MustCompile(`\d+`)

	for rowIndex, row := range data {
		for _, match := range r.FindAllIndex(row, -1) {
			numberStart := match[0]
			numberEnd := match[1] - 1
			fmt.Println(rowIndex, match)

			for gearRowIndex := max(rowIndex - 1, 0); gearRowIndex <= min(rowIndex + 1, len(data) - 1); gearRowIndex++ {
				gearRow := gears[gearRowIndex]
				for gearColumnIndex := max(numberStart - 1, 0); gearColumnIndex <= min(numberEnd + 1, len(gearRow) - 1); gearColumnIndex++ {
					gear := gearRow[gearColumnIndex]
					fmt.Println(gearRowIndex, gearColumnIndex)
					if gear[0] == 1 {
						//fmt.Println("Hereuka")
						// Now we have to get the number
						number := 0
						for rowIndexNumber := numberStart; rowIndexNumber <= numberEnd; rowIndexNumber++ {
							value := row[rowIndexNumber]
							number += int(value - '0') * int(math.Pow10(numberEnd - rowIndexNumber))
						}
						//fmt.Println(number)
						//fmt.Println(len(gear))

						gears[gearRowIndex][gearColumnIndex] = append(gear, number) 
					}
				}
			}
		}
	}

	result := 0
	for rowIndex, row := range gears {
		for columnIndex, column := range row {
			//fmt.Println(len(column))
			// Only allow gears with 2 number (+1 for signal)
			if len(column) == 3 {
				result += column[1] * column[2]
				fmt.Println(rowIndex, columnIndex, column[1], column[2])
			}
		}
	}
	fmt.Println(result)
}

