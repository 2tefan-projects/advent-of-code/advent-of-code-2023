package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
)

const LineLength = 140

func isNumber(value byte) bool {
	return '0' <= value && value <= '9'
}

func parseNumberLeft(line []byte, index int) int {
	out := 0
	for i := index; i >= 0; i-- {
		value := line[i]
		if isNumber(value) {
			out += int(value - '0') * int(math.Pow10(index - i))
			line[i] = '.'
		} else {
			break
		}

	}

	fmt.Println("Left: ", out)
	return out
}

func parseNumberRight(line []byte, index int) int {
	//outSli := make([]int, (len(line) - index) / 2) 
	outSli := []int{}
	// Slice instead of array because:
	// - array length int(index) (value of type int) must be constant
	// ;(

	for i := index; i < len(line); i++ {
		value := line[i]
		if isNumber(value) {
			outSli = append(outSli, int(value - '0'))
			line[i] = '.'
		}
	}

	out := 0
	for i := len(outSli) - 1; i >= 0; i-- {
		out += outSli[i] * int(math.Pow10(len(outSli) - i - 1))
	}
	fmt.Println("Right: ", out)
	return out
}

func parseNumberSearch(line []byte, index int) int {
	for i := index; i < len(line); i++ {
		value := line[i]
		if ! isNumber(value) {
			return parseNumberLeft(line, i - 1)
		}
	}

	return parseNumberLeft(line, len(line))
}

func parseNumberLine(line []byte, index int) int {
	isLeftNumber := 0 <= index - 1 && isNumber(line[index - 1])
	isMiddleNumber := isNumber(line[index])
	isRightNumber := len(line) > index + 1 && isNumber(line[index + 1])

	if isLeftNumber && isMiddleNumber && isRightNumber {
		return parseNumberSearch(line, index + 1)
	} else if isLeftNumber && isMiddleNumber && ! isRightNumber {
		return parseNumberSearch(line, index)
	} else if isLeftNumber && ! isMiddleNumber && isRightNumber {
		return parseNumberLeft(line, index - 1) + parseNumberRight(line, index + 1) 
	} else if isLeftNumber && ! isMiddleNumber && ! isRightNumber {
		return parseNumberLeft(line, index - 1)
	} else if ! isLeftNumber && isMiddleNumber && isRightNumber {
		return parseNumberRight(line, index)
	} else if ! isLeftNumber && isMiddleNumber && ! isRightNumber {
		return parseNumberLeft(line, index)
	} else if ! isLeftNumber && ! isMiddleNumber && isRightNumber {
		return parseNumberRight(line, index + 1)
	} else {
		return 0
	}
}



func main() {
	reader := bufio.NewReaderSize(os.Stdin, LineLength)
	var lastLine []byte
	validLocations := []int{}
	var sum uint32 = 0

	for {
		line, err := reader.ReadBytes('\n')

		if err != nil {
			fmt.Println("Done reading, does not end with a newline at the end!")
			break
		}

		fmt.Println(line)

		for _, v := range validLocations {
			fmt.Println("Next line")
			sum += uint32(parseNumberLine(line, v))
		}

		validLocations = []int{}

		for i, v := range line {
			if v != '.' && v != '\n' && ! isNumber(v) {
				fmt.Println("Found symbol: ", i, v)

				fmt.Println("Last line")
				sum += uint32(parseNumberLine(lastLine, i))

				validLocations = append(validLocations, i)
				//fmt.Printf("%08b\n", validLocations)

				fmt.Println("Current line")
				if isNumber(line[i-1]) {
					sum += uint32(parseNumberLeft(line, i-1))
				}

				if isNumber(line[i+1]) {
					sum += uint32(parseNumberRight(line, i+1))
				}
			}
		}

		lastLine = line
	}

	fmt.Println(sum)
}

