package main

import (
	"fmt"
	"os"
	"strings"
	"bufio"
	"regexp"
	"strconv"
)

func spelledOutStringToNumber(input string) int {
	switch input {
	case "one":
		return 1
	case "two":
		return 2
	case "three":
		return 3
	case "four":
		return 4
	case "five":
		return 5
	case "six":
		return 6
	case "seven":
		return 7
	case "eight":
		return 8
	case "nine":
		return 9
	}

	return 0
}

func main() {
	reader := bufio.NewReader(os.Stdin)

	sum := 0
	r := regexp.MustCompile(`one|two|three|four|five|six|seven|eight|nine|\d`)

	for {
		// read line from stdin using newline as separator
		line, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println("Something bad happened, I am going to exit now...")
			os.Exit(1)
		}

		if len(strings.TrimSpace(line)) == 0 {
			break
		}

		fmt.Print(line)

		matches := r.FindAllString(line, -1)

		firstValue := matches[0]
		lastValue := matches[len(matches) - 1]

		first, err := strconv.Atoi(firstValue)
		if err != nil {
			first = spelledOutStringToNumber(firstValue)
		}
		last, err := strconv.Atoi(lastValue)
		if err != nil {
			last = spelledOutStringToNumber(lastValue)
		}

		fmt.Printf("%d%d\n", first, last)

		//fmt.Println(first)
		//fmt.Println(last)
		sum += first * 10 + last
	}

	fmt.Println(sum)
}
