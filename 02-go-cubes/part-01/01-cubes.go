package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strings"
	"strconv"
)


const numberRedCubes = 12
const numberGreenCubes = 13
const numberBlueCubes = 14


func main() {
	reader := bufio.NewReader(os.Stdin)
	r := regexp.MustCompile(`^ (?P<count>\d+) (?P<type>green|blue|red)\n?$`)

	sum := 0
	gameId := 0

	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			break
		}

		gameId++

		line = strings.ToLower(line)
		data := strings.Split(line, ":")
		sets := strings.Split(data[1], ";")

		isValidSet := true

		for _, set := range sets {
			fmt.Println(set)

			cubes := strings.Split(set, ",")

			for _, cube := range cubes {
				fmt.Println(cube)
				matches := r.FindStringSubmatch(cube)

				color := matches[r.SubexpIndex("type")]
				count, _ := strconv.Atoi(matches[r.SubexpIndex("count")])

				if color == "red" && count > numberRedCubes ||
				color == "blue" && count > numberBlueCubes ||
				color == "green" && count > numberGreenCubes {
					isValidSet = false
					break
				}
			}

			if ! isValidSet {
				break
			}
		}

		if isValidSet {
			sum += gameId
		}
	}

	fmt.Println(sum)
}
