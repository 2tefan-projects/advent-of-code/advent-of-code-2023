package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
)

const LineLength = 140

func isNumber(value byte) bool {
	return '0' <= value && value <= '9'
}

func parseNumber(line []byte, validFields []bool, index int) int {
	out := 0
	for i := index; i >= 0; i-- {
		value := line[i]
		lastValue := line[min(i+1,len(line)-1)]
		isValid := validFields[i]
		lastIsValid := validFields[min(i + 1, len(line) - 1)]

		//if  ! ( isValid || (lastIsValid && isNumber(lastValue))) {
		if  ! (( isValid || (lastIsValid && isNumber(lastValue))) && isNumber(value)) {
			//fmt.Printf("  %c  ", value)
			out += parseNumber(line, validFields, i - 1)
			return out
		}

		out += int(value - '0') * int(math.Pow10(index - i))
		validFields[i] = true
		//fmt.Printf("%d %d -> [ %c ]", index, i, value)
		//fmt.Printf("[ %c ]", value)
	}

	return out
}

func parseNumberInit(line []byte, validFields []bool) int {
	if len(validFields) != len(line) {
		fmt.Println("Invalid length of arrays!", len(validFields), len(line))
		return 0
	}


	for i := 1; i < len(line); i++ {
		value := line[i]
		lastValue := line[i - 1]
		lastIsValid := validFields[i - 1]

		if  ! (isNumber(lastValue) && lastIsValid && isNumber(value)) {
			continue
		}

		validFields[i] = true
	}


	//fmt.Print("[ ")
	result := parseNumber(line, validFields, len(line) - 1)
	fmt.Println(result)
	return result
}

func min(i1 int, i2 int) int {
	if i1 > i2 {
		return i2
	}
	return i1
}

func max(i1 int, i2 int) int {
	if i1 < i2 {
		return i2
	}
	return i1
}

func setRangeToValue(arr []bool, value bool, start int, end int) {
	indexStart := max(start, 0)
	indexEnd := min(end, len(arr) - 1)

	for i := indexStart; i <= indexEnd; i++ {
		arr[i] = true
	}
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, LineLength)
	var lastLine []byte
	var sum uint32 = 0

	var lastValidLocations []bool
	var currentValidLocations []bool
	var nextValidLocations []bool
	for {
		line, err := reader.ReadBytes('\n')

		if err != nil {
			// Empty line at the end
			line = make([]byte, 0, len(lastLine))
		}

		if currentValidLocations == nil {
			lastValidLocations = make([]bool, len(line))
			currentValidLocations = make([]bool, len(line))
			nextValidLocations = make([]bool, len(line))
		}
		//fmt.Printf("%s", line)

		for i, v := range line {
			if v != '.' && v != '\n' && ! isNumber(v) {
				//fmt.Println("Found symbol: ", i, v)

				setRangeToValue(lastValidLocations, true, i-1, i+1)
				setRangeToValue(nextValidLocations, true, i-1, i+1)
				currentValidLocations[max(i - 1, 0)] = true
				currentValidLocations[min(i + 1, len(currentValidLocations) - 1)] = true
			}
		}

		//fmt.Println(lastValidLocations)
		//fmt.Println(currentValidLocations)
		//fmt.Println(nextValidLocations)


		fmt.Printf("%s", lastLine)
		sum += uint32(parseNumberInit(lastLine, lastValidLocations))

		lastValidLocations = currentValidLocations
		currentValidLocations = nextValidLocations
		nextValidLocations = make([]bool, len(line))
		lastLine = line

		if err != nil {
			break
		}
	}

	fmt.Println(sum)
}

