# src: https://www.reddit.com/r/adventofcode/comments/189m3qw/comment/kbs9g3g
# author: 4HbQ

import math as m, re
import fileinput

board = list(line for line in fileinput.input())
chars = {(r, c): [] for r in range(140) for c in range(140)
                    if board[r][c] not in '0123456789.'}

for r, row in enumerate(board):
    for n in re.finditer(r'\d+', row):
        edge = {(r, c) for r in (r-1, r, r+1)
                       for c in range(n.start()-1, n.end()+1)}

        for o in edge & chars.keys():
            chars[o].append(int(n.group()))


for key, value in chars.items():
    if len(value) == 2:
        print(key[0], key[1], value[0], value[1])

print(sum(sum(p)    for p in chars.values()),
      sum(m.prod(p) for p in chars.values() if len(p)==2))