package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)


const numberRedCubes = 12
const numberGreenCubes = 13
const numberBlueCubes = 14

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}


func main() {
	reader := bufio.NewReader(os.Stdin)
	r := regexp.MustCompile(`^ (?P<count>\d+) (?P<type>green|blue|red)\n?$`)

	sum := 0
	gameId := 0

	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			break
		}

		gameId++

		line = strings.ToLower(line)
		data := strings.Split(line, ":")
		sets := strings.Split(data[1], ";")
		requiredRed := 0
		requiredGreen := 0
		requiredBlue := 0

		for _, set := range sets {
			fmt.Println(set)

			cubes := strings.Split(set, ",")

			for _, cube := range cubes {
				fmt.Println(cube)
				matches := r.FindStringSubmatch(cube)

				color := matches[r.SubexpIndex("type")]
				count, _ := strconv.Atoi(matches[r.SubexpIndex("count")])

				if color == "red"{
					requiredRed = max(requiredRed, count)
				} else if color == "blue" {
					requiredBlue = max(requiredBlue, count)
				} else {
					requiredGreen = max(requiredGreen, count)
				}
			}
		}
	
		cubePower := requiredRed * requiredBlue * requiredGreen
		fmt.Println(cubePower)

		sum += cubePower
	}

	fmt.Println(sum)
}
