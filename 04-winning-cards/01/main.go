package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	totalScore := 0

	for scanner.Scan() {
		tokens := strings.Split(scanner.Text(), "|")
		winningNumbes := strings.Fields(strings.Split(tokens[0], ":")[1])
		myNumbers := strings.Fields(tokens[1])

		fmt.Println("Winning cards: ", winningNumbes, " // My cards: ", myNumbers)

		matches := 0
		for indexWinnigNumbers := 0; indexWinnigNumbers < len(winningNumbes); indexWinnigNumbers++ {
			currentWinningNumber := winningNumbes[indexWinnigNumbers]
			fmt.Println("Searching in my cards for", currentWinningNumber)

			for indexMyNumbers := 0; indexMyNumbers < len(myNumbers); indexMyNumbers++ {
				currentMyNumber := myNumbers[indexMyNumbers]

				if currentMyNumber == currentWinningNumber {
					println("Yay!")
					matches++
				}
			}

		}
		fmt.Println(matches)
		if matches > 1 {
			totalScore += 2 << (matches - 2)
		} else if matches == 1 {
			totalScore++
		}
		fmt.Println("Score after this line: ", totalScore)
	}

	fmt.Println("Total score 🎉: ", totalScore)

}
