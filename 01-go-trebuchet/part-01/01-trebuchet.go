package main

import (
	"fmt"
	"os"
	"io"
)

func main() {
	data, _ := io.ReadAll(os.Stdin)
	//fmt.Println(data)

	sum := 0
	var firstDigit byte
	var lastDigit byte

	for i := len(data) - 1; i >= 0; i-- {
		char := data[i]

		if (char == 10) {
			tempLineNumber := firstDigit * 10 + lastDigit
			fmt.Printf("New line detected -> number of previous line was: %d\n", tempLineNumber)
			sum += int(tempLineNumber)
			firstDigit = 0
			lastDigit = 0
		}

		if ! (48 <= char && char <= 57) {
			continue
		}

		number := char - 48
		firstDigit = number

		if lastDigit == 0 {
			lastDigit = number
		}
	}

	tempLineNumber := firstDigit * 10 + lastDigit
	sum += int(tempLineNumber)
	fmt.Println(sum)
}
